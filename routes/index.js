var express = require('express');
var multer = require('multer')
var router = express.Router();
const path = require('path');
const fs = require('fs');
const { body } = require('express-validator');

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    const dir = 'public/images';
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
    cb(null, dir)
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + '-bagus' + path.extname(file.originalname))
  }
})

var upload = multer({
  storage: storage,
  fileFilter: function (req, file, callback) {
    var ext = path.extname(file.originalname);
    if (ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg') {
      return callback(new Error('Only images are allowed'))
    }
    callback(null, true)
  },
})

const UserController = require('../controllers/user');
const loginController = require('../controllers/login');
const AmzarController = require('../controllers/amzar');
const passport = require('passport');

const userController = new UserController('IhsanTelecomunication');
const amzarController = new AmzarController();

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get(
  '/api/user',
  passport.authenticate('jwt', { 'session': false }),
  userController.list
);

router.post(
  '/api/user',
  body('email').isEmail(),
  body('password').isLength({ min: 5, max: 10 }),
  body('nohp').matches(/^(\+62|62|0)8[1-9][0-9]{6,11}$/),
  userController.create
);

router.post('/api/login', loginController.login);
router.post('/api/refresh-token', loginController.refresh);

router.post('/api/avatar',
  upload.single('gambar'),
  userController.avatar
);

router.get('/amzar', amzarController.view);
router.get('/input', amzarController.formInput);
router.post('/input-name', amzarController.input);

module.exports = router;

