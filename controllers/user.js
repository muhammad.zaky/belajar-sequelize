const User = require('../models').User;
const WorkingDay = require('../models').WorkingDay;
const bcrypt = require('bcrypt');
const { validationResult } = require('express-validator');
class UserController {
  constructor(company) {
    this.perusahaan = company || 'TELKOMSEL';
    this.list = this.list.bind(this);
    this.create = this.create.bind(this);
  }

  async list(req, res) {
    try {
      const user = await User.findAll({
        include: ['company', {
          model: WorkingDay,
          as: 'workingDays',
          attributes: ["id", "weekDay", "workingDate", "isWorking"],
          through: {
            attributes: [],
          }
        }]
      });

      if (user.length === 0) {
        res.status(400).send({
          error: "not found user"
        })
      }

      res.status(200).send({
        message: `Selamat datang mas ${req.user.name}`,
        data: user,
        company: this.perusahaan,
      });
    } catch (error) {
      res.status(500).send(error);
    }
  }

  async create(req, res) {
    try {
      const errors = validationResult(req);
      console.log('errors', errors);
      if (!errors.isEmpty()) {
        res.status(400).send({
          message: "error form validation",
          error: errors
        })
      }


      const password = req.body.password;
      // const saltRounds = 10;

      // const salt = bcrypt.genSaltSync(saltRounds);
      // const passwordHashed = bcrypt.hashSync(password, salt);

      await User.create({
        email: req.body.email,
        name: req.body.name,
        companyId: req.body.companyId,
        password: password
      })

      res.status(200).send({
        message: 'yeaaaa, you sucess insert data!!!'
      })
    } catch (error) {
      res.status(500).send({
        message: 'you wrong!!!'
      })
    }
  }

  async avatar(req, res) {
    const name = req.body.name;
    const email = req.body.email;
    const image = req.file.filename;
    const imagePath = req.file.path;
    // sugeng
    // const truePath = imagePath.split('/');
    // const imageField = truePath.slice(1).join('/');
    // console.log(req);

    // images/1612859493610-bagus.png
    // gede
    // var truePath = imagePath.replace('public/', '');

    // nanda
    // const panjang = imagePath.lenght
    // const result = imagePath.substring(7, panjang);

    // handoko
    // const result = imagePath.slice(imagePath.indexOf("/") + 1, imagePath.length)

    // amzar & fajri
    const imageTrue = imagePath.split('/');
    delete imageTrue[0];
    const result = imageTrue.join('/');

    res.status(200).send({
      name,
      email,
      image,
      imagePath,
      truePath: result,
    })
  }
}

module.exports = UserController;