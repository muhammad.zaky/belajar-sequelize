const User = require('../models').User;
const WorkingDay = require('../models').WorkingDay;

class user {
  async list(req, res) {
    console.log('req', req)
    try {
      const user = await User.findAll({
        include: ['company', {
          model: WorkingDay,
          as: 'workingDays',
          attributes: ["id", "weekDay", "workingDate", "isWorking"],
          through: {
            attributes: [],
          }
        }]
      });
      res.status(200).send({
        message: `Selamat datang mas ${req.user.name}`,
        data: user
      });
    } catch (error) {
      res.status(500).send(error);
    }
  }
}

module.exports = user;