class AmzarController {
  async view(req, res) {
    const name = req.query.nama;
    const jabatan = req.query.jabatan;
    const data = {
      name: [
        'krisna arizona',
        'Ratu Diana',
        'Pangeran Charles'
      ],
      ketua: 'handoko',
      newName: name,
      jabatan
    };
    res.render('amzar', data);
  }

  async formInput(req, res) {
    res.render('forminput');
  }

  async input(req, res) {
    const name = req.body.nama;
    const jabatan = req.body.jabatan;
    // simpan ke db
    const data = {
      name,
      jabatan
    }

    res.render('hasil-input', data);
  }
}

module.exports = AmzarController;