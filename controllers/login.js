const UserModel = require('../models').User;
const refreshTokenModel = require('../models').refreshtokens;
const JWT = require('jsonwebtoken');
const bcrypt = require('bcrypt');

module.exports = {
  async login(req, res) {
    try {
      const email = req.body.email;
      const password = req.body.password;

      const user = await UserModel.findOne({
        where: {
          email: email
        }
      })

      if (user === null) {
        res.status(400).send({
          message: "your email is wrong",
        })
      }

      const passwordUser = user.password;
      const comparePassword = bcrypt.compareSync(password, passwordUser);
      if (!comparePassword) {
        res.status(401).send({
          message: "you failed login"
        })
      }

      const token = JWT.sign(
        { user },
        process.env.SECRETKEY
      );

      const refreshToken = JWT.sign(
        { user },
        process.env.SECRETKEYREFRESH
      )

      await refreshTokenModel.create({
        refreshToken: refreshToken,
      })

      res.status(200).send({
        message: "your email is right",
        token,
        refreshToken
      });

    } catch (error) {
      console.log('error', error)
      res.status(500).send({
        message: "email salah"
      })
    }
  },

  async refresh(req, res) {
    const refreshTokenFromUser = req.body.refreshToken;
    const decoded = JWT.verify(refreshTokenFromUser, process.env.SECRETKEYREFRESH);
    console.log('decoded', decoded);

    const refreshTokenInDB = await refreshTokenModel.findAll({
      where: {
        refreshToken: refreshTokenFromUser
      }
    })

    if (refreshTokenInDB.length > 0) {
      const user = {
        email: "zaky@ganteng.com",
        name: "zaky"
      }

      const token = JWT.sign(
        { user },
        process.env.SECRETKEY
      );

      const refreshToken = JWT.sign(
        { user },
        process.env.SECRETKEYREFRESH
      )

      await refreshTokenModel.create({
        refreshToken: refreshToken,
      })

      res.status(200).send({
        message: "your email is right",
        token,
        refreshToken
      });
    }

    res.status(500).send({
      message: "email salah"
    })
  }
}