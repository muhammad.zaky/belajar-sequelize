var express = require('express');
var router = express.Router();

const MovieController = require('./movieController');
const movieController = new MovieController();

router.get('/', movieController.list);

module.exports = router;