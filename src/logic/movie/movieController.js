class MovieController {
  async list(req, res) {
    const dataMovie = [
      {
        name: 'raja bali',
        actor: 'Mas Gede'
      },
      {
        name: 'menerawang bumi',
        actor: 'Amzar'
      }
    ];

    res.status(200).send(dataMovie);
  }
}

module.exports = MovieController;