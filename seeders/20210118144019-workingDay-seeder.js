'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.bulkInsert('WorkingDays', [{
        weekDay: 'Senin',
        workingDate: '2020-01-01',
        isWorking: true,
        createdAt: new Date(),
        updatedAt: new Date(),
      }], {});
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('WorkingDays', null, {});
  }
};
