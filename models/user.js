'use strict';
const bcrypt = require('bcrypt');
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    email: {
      type: DataTypes.TEXT,
      get(email) {
        return this.getDataValue(email) + '@telkomsel.com'
      },
      // set(email) {
      //   this.setDataValue('email', email + '@telkomsel.com');
      // }
    },
    name: {
      type: DataTypes.STRING,
    },
    companyId: DataTypes.INTEGER,
    password: {
      type: DataTypes.STRING,
      set(password) {
        const saltRounds = 10;

        const salt = bcrypt.genSaltSync(saltRounds);
        const passwordHashed = bcrypt.hashSync(password, salt);
        this.setDataValue('password', passwordHashed);
      }
    }
  }, {});
  User.associate = function (models) {
    User.belongsTo(models.Company, {
      foreignKey: 'companyId',
      as: 'company'
    });
    User.belongsToMany(models.WorkingDay, {
      through: 'UserWorkingDays',
      foreignKey: 'userId',
      as: 'workingDays'
    })
  };
  return User;
};