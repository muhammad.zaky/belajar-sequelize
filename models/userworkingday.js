'use strict';
module.exports = (sequelize, DataTypes) => {
  const UserWorkingDay = sequelize.define('UserWorkingDay', {
    userId: DataTypes.INTEGER,
    workingDayId: DataTypes.INTEGER
  }, {});
  UserWorkingDay.associate = function(models) {
    // associations can be defined here
    // UserWorkingDay.belongsTo(models.User, {
    //   foreignKey: 'userId',
    //   as: 'user'
    // });
    // UserWorkingDay.belongsTo(models.WorkingDay, {
    //   foreignKey: 'workingDayId',
    //   as: 'day'
    // })
  };
  return UserWorkingDay;
};
