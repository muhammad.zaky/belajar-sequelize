'use strict';
module.exports = (sequelize, DataTypes) => {
  const refreshtoken = sequelize.define('refreshtokens', {
    refreshToken: DataTypes.TEXT
  }, {});
  refreshtoken.associate = function (models) {
    // associations can be defined here
  };
  return refreshtoken;
};