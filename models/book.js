'use strict';
module.exports = (sequelize, DataTypes) => {
  const Book = sequelize.define('Book', {
    name: DataTypes.STRING,
    author_id: DataTypes.INTEGER,
    category: DataTypes.STRING
  }, {});
  Book.associate = function(models) {
      Book.belongsTo(models.Author, {
        foreignKey: 'author_id',
        as: 'author'
      })
  };
  return Book;
};
