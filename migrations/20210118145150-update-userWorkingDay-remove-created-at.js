'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('UserWorkingDays', 'updatedAt'),
      queryInterface.removeColumn('UserWorkingDays', 'createdAt')
    ])
  },

  down: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
        'UserWorkingDays',
        'updatedAt',
        {
          type: Sequelize.DATE,
        }
      ),
      queryInterface.addColumn(
        'UserWorkingDays',
        'createdAt',
        {
          type: Sequelize.DATE
        }
      ),

    ])
  }
};
