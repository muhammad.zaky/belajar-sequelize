// Import the dependencies for testing
const chai = require('chai')
const chaiHttp = require('chai-http')
const sinon = require('sinon');
const app = require('../../app')
const User = require('../../models').User;


const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImVtYWlsIjoiYmFndXNAdGVsa29tc2VsLmNvbUB0ZWxrb21zZWwuY29tIiwiaWQiOjgsIm5hbWUiOiJiYWd1cyBzZXR5YXdhbiIsImNvbXBhbnlJZCI6MSwicGFzc3dvcmQiOiIkMmIkMTAkTUhRNGV3TXJ3UWpKaGdGVloub2lXdWxWYkEyNnpVNDVXLll1TmVERzRYUUxBT04wOHh6M1MiLCJjcmVhdGVkQXQiOiIyMDIxLTAxLTIxVDA2OjU2OjIyLjM0NVoiLCJ1cGRhdGVkQXQiOiIyMDIxLTAxLTIxVDA2OjU2OjIyLjM0NVoifSwiaWF0IjoxNjExMjE0NDQxfQ.AdRfd-Szye8QvW_Z36FFwzTuhWgz0LvBzsUEnQTVJF0';

// Configure chai
chai.use(chaiHttp);
chai.should();

describe("User", () => {
  beforeEach(function () {
    sinon.restore();
  });

  describe("GET /api/user", () => {

    it("harus mereturn hasil berupa daftar user", (done) => {
      chai.request(app)
        .get('/api/user')
        .set({ "Authorization": `Bearer ${token}` })
        .end((err, res) => {
          console.log('ini hasilnya', res.body);
          res.should.have.status(200);
          res.body.should.to.be.an('object');
          res.body.should.to.have.property('message');
          res.body.should.to.have.property('data');
          res.body.should.to.have.property('company');
          done();
        });
    });

    it("harus mereturn error 400 karena user tidak ada", (done) => {
      sinon.stub(User, "findAll").callsFake(() => {
        return []
      })

      chai.request(app)
        .get('/api/user')
        .set({ "Authorization": `Bearer ${token}` })
        .end((err, res) => {
          console.log('ini hasilnya', res.body);
          res.should.have.status(400);
          done();
        });
    });

    it("harus mereturn error 500", (done) => {
      sinon.stub(User, "findAll").throws(new Error('error database'));


      chai.request(app)
        .get('/api/user')
        .set({ "Authorization": `Bearer ${token}` })
        .end((err, res) => {
          console.log('ini hasilnya', res.body);
          res.should.have.status(500);
          done();
        });
    });
  });
  describe("POST /api/user", () => {

  })
});