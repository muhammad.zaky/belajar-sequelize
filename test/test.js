// Import the dependencies for testing
const chai = require('chai')
const chaiHttp = require('chai-http')
const app = require('../app')

// Configure chai
chai.use(chaiHttp);
chai.should();

describe("mengetes url utama", () => {
  describe("mengetes url GET /user", () => {
    // Test to get all students record
    it("hasilnya harus benar dengan keluar status 200", (done) => {

      chai.request(app)
        .get('/')
        .end((err, res) => {
          res.should.have.status(200);
          done();
        });
    });
  });
});